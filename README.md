# mua

#### 项目介绍
- 项目名称：mua
- 所属系列：openharmony的第三方组件适配移植
- 功能：支持多语言 支持GFM Markdown 语法说明 工具栏，用于插入Markdown代码、图片、加粗、斜体等等 菜单操作，用于保存、重命名、删除等 文件搜索 MIT协议
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示

![输入图片说明](https://gitee.com/chinasoft2_ohos/mua/raw/master/gif/demo.gif "demo.gif")

#### 安装教程
无

#### 使用说明
添加标题

```java
int start = editText.getStartPosition();
        StringBuilder stringBuilder = new StringBuilder();
        String textContent = editText.getText();
        int lineBreakPos = textContent.lastIndexOf("\n", start);
        int insertPos;
        if (lineBreakPos == -INT_1) {
            insertPos = 0;
        } else {
            insertPos = lineBreakPos + 1;
        }
        stringBuilder.append(editText.getText());
        stringBuilder.insert(insertPos, "#");
        String afterInsert = "";
        if (insertPos != 0) {
            if (insertPos < editText.getText().length()) {
                afterInsert = editText.getText().substring(insertPos + 1);
            }
        }
        if (!afterInsert.startsWith("#") && !afterInsert.startsWith(" ")) {
            stringBuilder.insert(insertPos + 1, " ");
        }
        editText.setText(stringBuilder.toString().replaceAll("# #", "##"));
        editText.setStartPosition(editText.getText().length());
        editText.setEndPosition(editText.getText().length());
```

字体加粗

```java
int start = editText.getStartPosition();
        int end = editText.getEndPosition();
        StringBuilder stringBuilder = new StringBuilder();
        if (start == end) {
            stringBuilder.append(editText.getText());
            stringBuilder.insert(start, "****");
        } else {
            stringBuilder.append(editText.getText());
            stringBuilder.insert(start, "**");
            stringBuilder.insert(end + INT_2, "****");
        }
        editText.setText(stringBuilder.toString());
        editText.setStartPosition(editText.getText().length());
        editText.setEndPosition(editText.getText().length());
```



#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息


Under MIT license, check the [license file](https://gitee.com/chinasoft2_ohos/mua/blob/master/LICENSE) for more detail.
