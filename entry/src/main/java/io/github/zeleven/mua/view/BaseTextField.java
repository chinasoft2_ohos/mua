package io.github.zeleven.mua.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.KeyEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseTextField
 *
 * @since 2021-05-11
 */
public class BaseTextField extends TextField implements Text.TextObserver, Component.KeyEventListener {
    private static final int INT = 0x001234;
    protected int startPosition;
    protected int endPosition;
    private List<String> undoList = new ArrayList<>();
    private List<String> redoList = new ArrayList<>();
    /**
     * BaseTextField
     *
     * @param context
     */

    public BaseTextField(Context context) {
        super(context);
    }

    /**
     * BaseTextField
     *
     * @param context
     * @param attrSet
     */
    public BaseTextField(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     * BaseTextField
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public BaseTextField(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    public List<String> getUndoList() {
        return undoList;
    }

    public List<String> getRedoList() {
        return redoList;
    }

    /**
     * getUndoList
     *
     * @param position
     * @return String
     */
    public String getUndoList(int position) {
        return undoList.get(position);
    }

    /**
     * getRedoList
     *
     * @param position
     * @return String
     */
    public String getRedoList(int position) {
        return redoList.get(position);
    }

    /**
     * removeUndoList
     *
     * @param position
     */
    public void removeUndoList(int position) {
        undoList.remove(position);
    }

    /**
     * removeRedoList
     *
     * @param position
     */
    public void removeRedoList(int position) {
        redoList.remove(position);
    }

    /**
     * addRedoList
     *
     * @param s
     */
    public void addRedoList(String s) {
        redoList.add(s);
    }

    private void init() {
        addTextObserver(this::onTextUpdated);
        setKeyEventListener(this::onKeyEvent);
        setCursorChangedListener(new CursorChangedListener() {
            @Override
            public void onCursorChange(TextField textField, int i, int i1) {
                startPosition = i;
                endPosition = i1;
            }
        });
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    @Override
    public void onTextUpdated(String s, int i, int i1, int i2) {
        for (String s1 : undoList) {
            if (s1.equals(s)) {
                return;
            }
        }
        if (undoList.size() != 0) {
            if (!undoList.get(undoList.size() - 1).equals(s)) {
                undoList.add(s);
            }
        } else {
            undoList.add(s);
        }
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
        if (component.hasFocus()) {
            if (keyEvent.isKeyDown()) {
                if (keyEvent.getKeyCode() == keyEvent.KEY_ENTER) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(getText());
                    stringBuilder.insert(startPosition, "\n");
                    setText(stringBuilder.toString());
                    setStartPosition(getText().length());
                    setEndPosition(getText().length());
                }
            }
        }
        return false;
    }
}