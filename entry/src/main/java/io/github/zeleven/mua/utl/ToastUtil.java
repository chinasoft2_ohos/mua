/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

import io.github.zeleven.mua.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * toast工具类，可快速实现toast效果，支持设置圆角弧度，x，y轴偏移和权重方式
 *
 * @since 2021-04-19
 */
public class ToastUtil {
    private static final int INT_58 = 58;
    private static final int INT_5000 = 5000;
    private static final int INT_3000 = 3000;
    private static int radius = INT_58;
    private Context context;

    /**
     * Toast构造器
     *
     * @param context
     */
    public ToastUtil(Context context) {
        this.context = context;
    }

    /**
     * 支持静态方法，传入上下文和需要显示的文字
     *
     * @param ctx
     * @param text
     */
    public static void toastCtx(Context ctx, String text) {
        new ToastDialog(ctx)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(INT_5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入上下文和需要布局
     *
     * @param ctx
     * @param layout
     */
    public static void toastCtx(Context ctx, Component layout) {
        Component customToastLayout = (Component) LayoutScatter.getInstance(ctx).parse(layout.getId(), null, false);
        ToastDialog toastDialog = new ToastDialog(ctx);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
    }

    /**
     * 支持静态方法，传入需要显示的文字
     *
     * @param text
     */
    public void toast(String text) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAutoClosable(true)
                .setDuration(INT_5000)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入需要显示的文字和圆角角度
     *
     * @param text
     * @param duration
     */
    public void toast(String text,int duration) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setDuration(duration)
                .setCornerRadius(radius)
                .show();
    }

    /**
     * 支持静态方法，传入需要显示的文字，圆角，X轴偏移，Y轴偏移，权重
     *
     * @param text
     * @param duration
     * @param offsetX
     * @param offsetY
     * @param gravity
     */
    public void toast(String text,int duration,int offsetX,int offsetY,int gravity) {
        new ToastDialog(context)
                .setText(text)
                .setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setAlignment(gravity)
                .setDuration(duration)
                .setCornerRadius(radius)
                .setOffset(offsetX, offsetY)
                .show();
    }

    /**
     * 支持静态方法，传入上下文和需要显示的文字
     *
     * @param context
     * @param text
     */
    public static void toast(Context context, String text) {
        DirectionalLayout customToastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_toast_dialog_layout, null, false);
        Text textView = (Text) customToastLayout.findComponentById(ResourceTable.Id_toast_text);
        textView.setText(text);
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setDuration(INT_3000);
        toastDialog.show();
    }

    /**
     * 支持静态方法，传入上下文和需要显示的文字
     *
     * @param context
     * @param text
     */
    public static void toastBlack(Context context, String text) {
        DirectionalLayout customToastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_toast_dialog_black_layout, null, false);
        Text textView = (Text) customToastLayout.findComponentById(ResourceTable.Id_toast_text);
        textView.setText(text);
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(customToastLayout);
        toastDialog.setCornerRadius(radius);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setDuration(INT_3000);
        toastDialog.show();
    }
}
