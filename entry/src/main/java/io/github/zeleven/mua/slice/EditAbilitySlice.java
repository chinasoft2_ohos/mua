package io.github.zeleven.mua.slice;

import io.github.zeleven.mua.ResourceTable;
import io.github.zeleven.mua.bean.EditCallback;
import io.github.zeleven.mua.bean.FileBean;
import io.github.zeleven.mua.fraction.EditFragment;
import io.github.zeleven.mua.fraction.HelpFraction;
import io.github.zeleven.mua.fraction.PreviewFragment;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.StackLayout;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * EditAbilitySlice
 *
 * @since 2021-05-11
 */
public class EditAbilitySlice extends AbilitySlice {
    private static final int INT = 0x001234;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FORE = 4;
    private HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, INT, "zf----");
    private String content = null;
    private String fileName = null;
    private StackLayout stackLayout;
    private EditCallback editCallback = new EditCallback() {
        @Override
        public void preview(int type, String s) {
            switch (type) {
                case 1:
                    ((FractionAbility) getAbility()).getFractionManager()
                            .startFractionScheduler()
                            .replace(ResourceTable.Id_show_stack, previewFragment)
                            .submit();
                    previewFragment.setValue(s);
                    break;
                case TWO:
                    ((FractionAbility) getAbility()).getFractionManager()
                            .startFractionScheduler()
                            .replace(ResourceTable.Id_show_stack, editFragment)
                            .submit();
                    break;
                case THREE:
                    onBackPressed();
                    break;
                case FORE:
                    present(new HelpFraction(), new Intent());
                    break;
                default:
                    break;
            }
        }
    };

    private EditFragment editFragment = new EditFragment(this, editCallback);
    private PreviewFragment previewFragment = new PreviewFragment(this, editCallback);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_edit);
        if (intent != null) {
            FileBean fileBean = (FileBean) intent.getSerializableParam("content");
            if (fileBean != null) {
                content = fileBean.getContent();
                fileName = fileBean.getTitle();
            }
        }
        initView();
    }

    private void initView() {
        stackLayout = (StackLayout) findComponentById(ResourceTable.Id_show_stack);

        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .replace(ResourceTable.Id_show_stack, editFragment)
                .submit();
        if (content != null) {
            editFragment.setContent(content);
            editFragment.setFileName(true);
            editFragment.setSave(true);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
