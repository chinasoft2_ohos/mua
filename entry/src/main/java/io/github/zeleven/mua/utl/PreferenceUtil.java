/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

import io.github.zeleven.mua.app.MyApplication;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * PreferenceUtil
 *
 * @since 2021-05-15
 */
public class PreferenceUtil {
    static PreferenceUtil preferenceUtils = null;
    private static final String PREFERENCE_FILE_NAME = "fileName";

    /**
     * 实例化
     *
     * @return PreferenceUtil
     */

    public static PreferenceUtil getInstance() {
        if (preferenceUtils == null) {
            preferenceUtils = new PreferenceUtil();
        }
        return preferenceUtils;
    }

    /**
     * getApplicationPref
     *
     * @param context
     * @return Preferences
     */

    Preferences getApplicationPref(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        return databaseHelper.getPreferences(PREFERENCE_FILE_NAME);
    }

    /**
     * 设置布尔值
     *
     * @param context
     * @param key
     * @param isValue
     */
    public void putBoolean(Context context, String key, boolean isValue) {
        preferenceUtils.getApplicationPref(context).putBoolean(key, isValue).flushSync();
    }

    /**
     * 获取 布尔值
     *
     * @param context
     * @param key
     * @param isDefValue
     * @return boolean
     */
    public boolean getBoolean(Context context, String key, boolean isDefValue) {
        return preferenceUtils.getApplicationPref(context).getBoolean(key, isDefValue);
    }

    /**
     * 设置字符串值
     *
     * @param key
     * @param value
     */
    public void putString(String key, String value) {
        preferenceUtils.getApplicationPref(MyApplication.getmContext()).putString(key, value).flushSync();
    }

    /**
     * 获取字符串值
     *
     * @param key
     * @param defValue 默认值（当获取的数据为null时，设置这个默认值）
     * @return String
     */
    public String getString(String key, String defValue) {
        return preferenceUtils.getApplicationPref(MyApplication.getmContext()).getString(key, defValue);
    }

    /**
     * putInt
     *
     * @param key
     * @param value
     */
    public void putInt(String key, int value) {
        preferenceUtils.getApplicationPref(MyApplication.getmContext()).putInt(key, value).flushSync();
    }

    /**
     * getInt
     *
     * @param key
     * @param value
     * @return int
     */
    public int getInt(String key, int value) {
        return preferenceUtils.getApplicationPref(MyApplication.getmContext()).getInt(key, value);
    }
}
