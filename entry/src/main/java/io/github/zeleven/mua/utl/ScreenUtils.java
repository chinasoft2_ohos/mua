/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

import static ohos.agp.components.AttrHelper.getDensity;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * ScreenUtils
 *
 * @since 2021-03-29
 */
public class ScreenUtils {
    /**
     * 获取屏幕的高度Px
     *
     * @param context
     * @return int
     */
    public static final int getHeightInPx(Context context) {
        final int height = context.getResourceManager().getDeviceCapability().height;
        return height;
    }

    /**
     * 获取屏幕的宽度Px
     *
     * @param context
     * @return int
     */
    public static final int getWidthInPx(Context context) {
        final int width = context.getResourceManager().getDeviceCapability().width;
        return width;
    }

    /**
     * 获取屏幕的高度Dp
     *
     * @param context
     * @return int
     */
    public static final int getHeightInDp(Context context) {
        final float height = context.getResourceManager().getDeviceCapability().height;
        int heightInDp = pxToFp(context, height);
        return heightInDp;
    }

    /**
     * 获取屏幕的宽度Dp
     *
     * @param context
     * @return int
     */
    public static final int getWidthInDp(Context context) {
        final float width = context.getResourceManager().getDeviceCapability().width;
        int widthInDp = fpToPx(context, width);
        return widthInDp;
    }

    /**
     * fp 转 Px
     *
     * @param context
     * @param fp
     * @return int
     */
    public static int fpToPx(final Context context, final float fp) {
        return AttrHelper.fp2px(fp, getDensity(context));
    }

    /**
     * px 转 Fp
     *
     * @param context
     * @param px
     * @return int
     */
    public static int pxToFp(final Context context, final float px) {
        return Math.round(px * getDensity(context));
    }

    /**
     * 获取当前设备属性
     *
     * @param context
     * @return Display
     */
    public static Display getDeviceAttr(Context context) {
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(context);
        return optional.get();
    }

    /**
     * 屏幕宽
     *
     * @param context
     * @return int
     */
    public static int getDisplayWidth(Context context) {
        return getDeviceAttr(context).getRealAttributes().width;
    }

    /**
     * 屏幕高
     *
     * @param context
     * @return int
     */
    public static int getDisplayHeight(Context context) {
        return getDeviceAttr(context).getRealAttributes().height;
    }

    /**
     * rgb颜色
     *
     * @param color 色值
     * @return RgbColor
     */
    private RgbColor getRgbColor(Color color) {
        return RgbColor.fromArgbInt(color.getValue());
    }
}
