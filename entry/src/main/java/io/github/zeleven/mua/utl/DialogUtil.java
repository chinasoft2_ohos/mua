/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

import io.github.zeleven.mua.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * DialogUtil
 *
 * @since 2021-03-29
 */
public class DialogUtil implements Component.ClickedListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    private static final int SIX = 6;
    private static final int SEVEN = 7;

    /**
     * DialogUtil
     *
     * @param context
     */
    public DialogUtil(AbilitySlice context) {
    }

    /**
     * 显示方法
     *
     * @param context
     * @param title
     * @param content
     */
    public static void show(Context context, String title, String content) {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(context).parse(ResourceTable.Layout_dialog_layout, null, false);
        Text mTitle = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_title);
        Text mContent = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_content);

        mTitle.setText(title);
        mContent.setText(content);

        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        customToastLayout.setLayoutConfig(layoutConfig);

        CommonDialog toastDialog = new CommonDialog(context);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(context) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();

        mContent.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.destroy();
            }
        });
    }

    @Override
    public void onClick(Component component) {
    }
}
