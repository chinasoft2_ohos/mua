/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.bean;

/**
 * StringCache
 *
 * @since 2021-05-15
 */
public class StringCache {
    private static final StringCache stringCache = new StringCache();
    // 繁体
    private static final int INT_0 = 0;
    private static final int INT_1 = 1;
    private static final int INT_2 = 2;
    private static final int INT_3 = 3;
    private static final String STRING = "";
    /**
     * Default
     *
     */
    public static final int Default = INT_0;
    /**
     * English
     *
     */
    public static final int English = INT_1;
    /**
     * Chinese
     *
     */
    public static final int Chinese = INT_2;
    /**
     * Chinesefont
     *
     */
    public static final int Chinesefont = INT_3;
    /**
     * string
     *
     */
    private String string = STRING;
    /**
     * fileName
     *
     */
    private String fileName = STRING;
    /**
     * LanguageType
     *
     */
    private int LanguageType = INT_1;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getLanguageType() {
        return LanguageType;
    }

    public void setLanguageType(int languageType) {
        LanguageType = languageType;
    }

    /**
     * getInstance
     *
     * @return StringCache
     */
    public static StringCache getInstance() {
        return stringCache;
    }
}
