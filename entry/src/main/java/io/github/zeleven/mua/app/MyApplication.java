package io.github.zeleven.mua.app;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

/**
 * MyApplication
 *
 * @since 2021-05-08
 */
public class MyApplication extends AbilityPackage {
    /**
     * mContext
     *
     */
    private static Context mContext;

    @Override
    public void onInitialize() {
        super.onInitialize();
        setContextStatic(getApplicationContext());
    }

    public static Context getmContext() {
        return mContext;
    }

    /**
     * static赋值
     *
     * @param ctx static type
     */
    public static void setContextStatic(final Context ctx) {
        mContext = ctx;
    }

}
