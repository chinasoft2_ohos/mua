package io.github.zeleven.mua.fraction;

import io.github.zeleven.mua.ResourceTable;
import io.github.zeleven.mua.bean.EditCallback;
import io.github.zeleven.mua.bean.JudgeBean;
import io.github.zeleven.mua.bean.StringCache;
import io.github.zeleven.mua.utl.PreferenceUtil;
import io.github.zeleven.mua.view.SlidingDrawer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

/**
 * AboutFraction
 *
 * @since 2021-05-11
 */
public class AboutFraction extends AbilitySlice implements Component.ClickedListener {
    private static final int INT_2 = 2;
    private static final int INT_3 = 3;
    private static final int INT_4 = 3;
    private Button btMe;
    private Button btLink;
    private Image back;
    private SlidingDrawer slide;
    private DirectionalLayout email;
    private DirectionalLayout git;
    private DirectionalLayout zhihu;
    private AbilitySlice slice;
    private Text describe;
    private Text version;

    /**
     * AboutFraction
     *
     * @param slice
     * @param callback
     */
    public AboutFraction(AbilitySlice slice, EditCallback callback) {
        this.slice = slice;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_fragment_about);
        btLink = (Button) findComponentById(ResourceTable.Id_bt_link);
        btMe = (Button) findComponentById(ResourceTable.Id_bt_me);
        back = (Image) findComponentById(ResourceTable.Id_ig_ic_back);
        slide = (SlidingDrawer) findComponentById(ResourceTable.Id_slide);
        email = (DirectionalLayout) findComponentById(ResourceTable.Id_email);
        git = (DirectionalLayout) findComponentById(ResourceTable.Id_git);
        zhihu = (DirectionalLayout) findComponentById(ResourceTable.Id_zhihu);
        describe = (Text) findComponentById(ResourceTable.Id_tx_app_describe);
        version = (Text) findComponentById(ResourceTable.Id_tx_app_version_text);
        back.setClickedListener(this);
        btMe.setClickedListener(this);
        btLink.setClickedListener(this);
        slide.setClickedListener(this);
        email.setClickedListener(this);
        git.setClickedListener(this);
        zhihu.setClickedListener(this);

        cutLanguage(btLink, ResourceTable.String_project_link, ResourceTable.String_en_project_link);
        cutLanguage(btMe, ResourceTable.String_contact_me, ResourceTable.String_en_pcontact_me);
        cutTextLanguage(describe, ResourceTable.String_app_describe, ResourceTable.String_en_app_describe);
        cutTextLanguage(version, ResourceTable.String_app_version_text, ResourceTable.String_en_app_version_text);
    }

    private void cutLanguage(Button text, int chResource, int enResource) {
        int anInt = PreferenceUtil.getInstance().getInt("int", 0);
        StringCache.getInstance().setLanguageType(anInt);
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            text.setText(slice.getString(chResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            text.setText(slice.getString(enResource));
        }
    }

    private void cutTextLanguage(Text text, int chResource, int enResource) {
        int anInt = PreferenceUtil.getInstance().getInt("int", 0);
        StringCache.getInstance().setLanguageType(anInt);
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            text.setText(slice.getString(chResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            text.setText(slice.getString(enResource));
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_ig_ic_back:
                onBackPressed();
                break;
            case ResourceTable.Id_bt_link:
                JudgeBean judgeBean = new JudgeBean();
                judgeBean.setType(1);
                present(new WbeFraction(judgeBean), new Intent());
                slide.animateClose();
                break;
            case ResourceTable.Id_bt_me:
                slide.animateOpen();
                break;
            case ResourceTable.Id_slide:
                break;
            case ResourceTable.Id_email:
                JudgeBean judgeBean2 = new JudgeBean();
                judgeBean2.setType(INT_2);
                present(new WbeFraction(judgeBean2), new Intent());
                slide.animateClose();
                break;
            case ResourceTable.Id_git:
                JudgeBean judgeBean3 = new JudgeBean();
                judgeBean3.setType(INT_3);
                present(new WbeFraction(judgeBean3), new Intent());
                slide.animateClose();
                break;
            case ResourceTable.Id_zhihu:
                JudgeBean judgeBean4 = new JudgeBean();
                judgeBean4.setType(INT_4);
                present(new WbeFraction(judgeBean4), new Intent());
                slide.animateClose();
                break;
            default:
                break;
        }
    }
}
