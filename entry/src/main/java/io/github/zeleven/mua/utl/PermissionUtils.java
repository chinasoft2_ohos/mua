/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

/**
 * PermissionUtils
 *
 * @since 2021-04-26
 */
public class PermissionUtils {
    /**
     * READ_MEDIA
     */
    public static final String READ_MEDIA = "ohos.permission.READ_MEDIA";
    /**
     * READ_USER_STORAGE
     */
    public static final String READ_USER_STORAGE = "ohos.permission.READ_USER_STORAGE";
    /**
     * WRITE_MEDIA
     */
    public static final String WRITE_MEDIA = "ohos.permission.WRITE_MEDIA";
    /**
     * CAMERA
     */
    public static final String CAMERA = "ohos.permission.CAMERA";
    /**
     * MEDIA_LOCATION
     */
    public static final String MEDIA_LOCATION = "ohos.permission.MEDIA_LOCATION";
    /**
     * WRITE_USER_STORAGE
     */
    public static final String WRITE_USER_STORAGE = "ohos.permission.WRITE_USER_STORAGE";
    /**
     * PERMISSION_CODE
     */
    public static final int PERMISSION_CODE = 0X000000;
    /**
     * 权限数组
     *
     * @return String[]
     */
    public String[] getPermissionUtilsLIST() {
        String[] plist = new String[]{
                READ_MEDIA, WRITE_MEDIA,
                MEDIA_LOCATION, CAMERA,
                WRITE_USER_STORAGE,READ_USER_STORAGE};
        return plist;
    }
}