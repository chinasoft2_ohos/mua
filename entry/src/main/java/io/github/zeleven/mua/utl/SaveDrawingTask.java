/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.utl;

import io.github.zeleven.mua.slice.EditAbilitySlice;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * SaveDrawingTask
 *
 * @since 2021-04-13
 */
public class SaveDrawingTask implements Runnable {
    private static final int INT = 0x001234;
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, INT, "zf------");
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "SaveDrawingTask");
    private static final String SAVED_IMAGE_FORMAT = ".md";
    private final WeakReference<EditAbilitySlice> activityWeakReference;
    private OnCompleteHandler onCompleteHandler;

    private String value;

    /**
     * SAVED_IMAGE_NAME
     *
     */
    private static String SAVED_IMAGE_NAME = "cutout_tmp";

    /**
     * SaveDrawingTask
     *
     * @param ability
     */
    public SaveDrawingTask(EditAbilitySlice ability) {
        this.activityWeakReference = new WeakReference<>(ability);
        EventRunner eventRunner = EventRunner.getMainEventRunner();
        setOnCompleteHandler(new OnCompleteHandler(eventRunner));
    }

    public static String getSavedImageName() {
        return SAVED_IMAGE_NAME;
    }

    public static void setSavedImageName(String savedImageName) {
        SAVED_IMAGE_NAME = savedImageName;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void run() {
        try {
            File file = new File(activityWeakReference.get().getApplicationContext().getExternalFilesDir(
                    Environment.DIRECTORY_DOCUMENTS), SAVED_IMAGE_NAME + SAVED_IMAGE_FORMAT);
            FileWriter outPutStream = new FileWriter(file);
            boolean isExists = file.exists();
            if (!isExists) {
                boolean newFile = file.createNewFile();
            }
            outPutStream.write(value.toCharArray());
            outPutStream.close();
        } catch (IOException e) {
            HiLog.error(LABEL, "IOException");
        }
    }

    /**
     * OnCompleteHandler
     *
     * @since 2021-04-13
     */
    public static class OnCompleteHandler extends EventHandler {
        OnCompleteHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            onComplete((ResultBean) event.object);
        }

        private void onComplete(ResultBean resultBean) {
            if (resultBean.isSuccess) {
                HiLog.info(hiLogLabel, "保存成功");
            } else {
                HiLog.info(hiLogLabel, "保存失败");
            }
        }
    }

    private void setOnCompleteHandler(OnCompleteHandler handler) {
        this.onCompleteHandler = handler;
    }

    private void complete(boolean isSuccess, File file) {
        if (onCompleteHandler != null) {
            int eventId = 0;
            long param = 0L;
            ResultBean resultBean = new ResultBean();
            resultBean.setSuccess(isSuccess);
            resultBean.setFile(file);
            InnerEvent innerEvent = InnerEvent.get(eventId, param, resultBean);
            onCompleteHandler.sendEvent(innerEvent);
        }
    }

    /**
     * ResultBean
     *
     * @since 2021-04-13
     */
    private static class ResultBean {
        boolean isSuccess;
        File file;

        public boolean isSuccess() {
            return isSuccess;
        }

        public void setSuccess(boolean isSu) {
            this.isSuccess = isSu;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }
    }
}
