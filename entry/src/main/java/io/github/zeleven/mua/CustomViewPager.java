package io.github.zeleven.mua;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * CustomViewPager
 *
 * @since 2021-05-11
 */
public class CustomViewPager extends PageSlider {
    /**
     * CustomViewPager
     *
     * @param context
     */
    public CustomViewPager(Context context) {
        super(context);
    }

    /**
     * CustomViewPager
     *
     * @param context
     * @param attrs
     */
    public CustomViewPager(Context context, AttrSet attrs) {
        super(context, attrs);
    }
}
