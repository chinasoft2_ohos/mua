package io.github.zeleven.mua.ability;

import io.github.zeleven.mua.slice.EditAbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

/**
 * EditAbility
 *
 * @since 2021-05-08
 */
public class EditAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(EditAbilitySlice.class.getName());
        onLeaveForeground();
    }
}
