/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.zeleven.mua.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 * FileBean
 *
 * @since 2021-05-15
 */
public class FileBean implements Serializable, Comparable<FileBean> {
    private String title;
    private String content;
    private int time;

    /**
     * FileBean
     *
     * @param title
     * @param content
     */
    public FileBean(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileBean fileBean = (FileBean) o;
        return time == fileBean.time &&
                title.equals(fileBean.title) &&
                content.equals(fileBean.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, content, time);
    }

    @Override
    public int compareTo(FileBean fileBean) {
        if (this.equals(fileBean)) {
            return 0;
        }
        int i = fileBean.getTime() - this.getTime();
        return i;
    }
}