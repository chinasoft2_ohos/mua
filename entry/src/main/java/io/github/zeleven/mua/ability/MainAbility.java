package io.github.zeleven.mua.ability;

import io.github.zeleven.mua.slice.MainAbilitySlice;
import io.github.zeleven.mua.utl.PermissionUtils;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

/**
 * MainAbility
 *
 * @since 2021-05-08
 */
public class MainAbility extends FractionAbility {
    private static final int INT = 996;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        PermissionUtils permissionUtils = new PermissionUtils();
        permissionUtils.getPermissionUtilsLIST();
        requestPermissionsFromUser(permissionUtils.getPermissionUtilsLIST(), INT);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
    }
}
