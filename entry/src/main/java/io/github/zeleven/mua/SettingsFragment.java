package io.github.zeleven.mua;

import io.github.zeleven.mua.bean.EditCallback;
import io.github.zeleven.mua.bean.JudgeBean;
import io.github.zeleven.mua.bean.SliceCallBack;
import io.github.zeleven.mua.bean.StringCache;
import io.github.zeleven.mua.fraction.AboutFraction;
import io.github.zeleven.mua.fraction.WbeFraction;
import io.github.zeleven.mua.utl.PreferenceUtil;
import io.github.zeleven.mua.utl.ScreenUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;

/**
 * SettingsFragment
 *
 * @since 2021-05-11
 */
public class SettingsFragment extends AbilitySlice implements Component.ClickedListener {
    private static final int SIX = 6;
    private static final int SEVEN = 7;
    private static final int INT_2 = 2;
    private static final int INT_5 = 5;
    private static final int INT_3 = 3;
    private Text prefkeyabout;
    private Text prefkeylanguage;
    private Image back;
    private AbilitySlice slice;
    private Text feedback;
    private Text report;
    private Image true1;
    private Image true2;
    private Image true3;
    private Image true4;
    private Image false1;
    private Image false2;
    private Image false3;
    private Image false4;
    private DependentLayout auto;
    private DependentLayout english;
    private DependentLayout chineses;
    private DependentLayout chineset;
    private CommonDialog languageDialog;
    private Text titile1;
    private Text titile2;
    private Text titile3;
    private Text titile4;

    /**
     * SettingsFragment
     *
     */
    public SettingsFragment() {
    }

    /**
     * SettingsFragment
     *
     * @param slice
     * @param callBack
     */
    public SettingsFragment(AbilitySlice slice, SliceCallBack callBack) {
        this.slice = slice;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_preferences);
        prefkeyabout = (Text) findComponentById(ResourceTable.Id_pref_key_about);
        prefkeylanguage = (Text) findComponentById(ResourceTable.Id_pref_key_language);
        back = (Image) findComponentById(ResourceTable.Id_ig_ic_back);
        feedback = (Text) findComponentById(ResourceTable.Id_feedback);
        report = (Text) findComponentById(ResourceTable.Id_report);
        Text display = (Text) findComponentById(ResourceTable.Id_tx_category_title_display);
        Text language = (Text) findComponentById(ResourceTable.Id_tx_pref_title_language);
        Text editor = (Text) findComponentById(ResourceTable.Id_tx_category_title_editor);
        Text customeditor = (Text) findComponentById(ResourceTable.Id_tx_pref_title_custom_editor);
        Text sync = (Text) findComponentById(ResourceTable.Id_tx_pref_title_sync);
        Text sync1 = (Text) findComponentById(ResourceTable.Id_tx_pref_title_sync1);
        Text summarysync = (Text) findComponentById(ResourceTable.Id_tx_pref_summary_sync);
        Text tt = (Text) findComponentById(ResourceTable.Id_tt);
        Text wifisync = (Text) findComponentById(ResourceTable.Id_tx_pref_summary_wifi_sync);
        Text titlefeedback = (Text) findComponentById(ResourceTable.Id_tx_category_title_feedback);
        Text titleothers = (Text) findComponentById(ResourceTable.Id_tx_category_title_others);
        Text titlerate = (Text) findComponentById(ResourceTable.Id_tx_pref_title_rate);
        Text checkupdate = (Text) findComponentById(ResourceTable.Id_tx_pref_title_check_update);
        Text versiontext = (Text) findComponentById(ResourceTable.Id_tx_app_version_text);
        Text settings = (Text) findComponentById(ResourceTable.Id_tx_settings);

        int anInt = PreferenceUtil.getInstance().getInt("int", 0);
        StringCache.getInstance().setLanguageType(anInt);
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            prefkeyabout.setText(slice.getString(ResourceTable.String_pref_title_about));
            prefkeylanguage.setText(slice.getString(ResourceTable.String_pref_summary_language));
            feedback.setText(slice.getString(ResourceTable.String_pref_title_feedback));
            report.setText(slice.getString(ResourceTable.String_pref_title_bug_report));
            display.setText(slice.getString(ResourceTable.String_category_title_display));
            language.setText(slice.getString(ResourceTable.String_pref_title_language));
            editor.setText(slice.getString(ResourceTable.String_category_title_editor));
            customeditor.setText(slice.getString(ResourceTable.String_pref_title_custom_editor));
            sync.setText(slice.getString(ResourceTable.String_pref_title_sync));
            sync1.setText(slice.getString(ResourceTable.String_pref_title_sync));
            summarysync.setText(slice.getString(ResourceTable.String_pref_summary_sync));
            tt.setText(slice.getString(ResourceTable.String_pref_title_wifi_sync));
            wifisync.setText(slice.getString(ResourceTable.String_pref_summary_wifi_sync));
            titlefeedback.setText(slice.getString(ResourceTable.String_category_title_feedback));
            titleothers.setText(slice.getString(ResourceTable.String_category_title_others));
            titlerate.setText(slice.getString(ResourceTable.String_pref_title_rate));
            checkupdate.setText(slice.getString(ResourceTable.String_pref_title_check_update));
            versiontext.setText(slice.getString(ResourceTable.String_app_version_text));
            settings.setText(slice.getString(ResourceTable.String_drawer_item_settings));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            prefkeyabout.setText(slice.getString(ResourceTable.String_en_pref_title_about));
            prefkeylanguage.setText(slice.getString(ResourceTable.String_en_pref_summary_language));
            feedback.setText(slice.getString(ResourceTable.String_en_pref_title_feedback));
            report.setText(slice.getString(ResourceTable.String_en_pref_title_bug_report));
            display.setText(slice.getString(ResourceTable.String_en_category_title_display));
            language.setText(slice.getString(ResourceTable.String_en_pref_title_language));
            editor.setText(slice.getString(ResourceTable.String_en_category_title_editor));
            customeditor.setText(slice.getString(ResourceTable.String_en_pref_title_custom_editor));
            sync.setText(slice.getString(ResourceTable.String_en_pref_title_sync));
            sync1.setText(slice.getString(ResourceTable.String_en_pref_title_sync));
            summarysync.setText(slice.getString(ResourceTable.String_en_pref_summary_sync));
            tt.setText(slice.getString(ResourceTable.String_en_pref_title_wifi_sync));
            wifisync.setText(slice.getString(ResourceTable.String_en_pref_summary_wifi_sync));
            titlefeedback.setText(slice.getString(ResourceTable.String_en_category_title_feedback));
            titleothers.setText(slice.getString(ResourceTable.String_en_category_title_others));
            titlerate.setText(slice.getString(ResourceTable.String_en_pref_title_rate));
            checkupdate.setText(slice.getString(ResourceTable.String_en_pref_title_check_update));
            versiontext.setText(slice.getString(ResourceTable.String_en_app_version_text));
            settings.setText(slice.getString(ResourceTable.String_en_drawer_item_settings));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.Chinesefont) {
            prefkeyabout.setText(slice.getString(ResourceTable.String_tw_pref_title_about));
            prefkeylanguage.setText(slice.getString(ResourceTable.String_tw_pref_summary_language));
            feedback.setText(slice.getString(ResourceTable.String_tw_pref_title_feedback));
            report.setText(slice.getString(ResourceTable.String_tw_pref_title_bug_report));
            display.setText(slice.getString(ResourceTable.String_tw_category_title_display));
            language.setText(slice.getString(ResourceTable.String_tw_pref_title_language));
            editor.setText(slice.getString(ResourceTable.String_tw_category_title_editor));
            customeditor.setText(slice.getString(ResourceTable.String_tw_pref_title_custom_editor));
            sync.setText(slice.getString(ResourceTable.String_tw_pref_title_sync));
            sync1.setText(slice.getString(ResourceTable.String_tw_pref_title_sync));
            summarysync.setText(slice.getString(ResourceTable.String_tw_pref_summary_sync));
            tt.setText(slice.getString(ResourceTable.String_tw_pref_title_wifi_sync));
            wifisync.setText(slice.getString(ResourceTable.String_tw_pref_summary_wifi_sync));
            titlefeedback.setText(slice.getString(ResourceTable.String_tw_category_title_feedback));
            titleothers.setText(slice.getString(ResourceTable.String_tw_category_title_others));
            titlerate.setText(slice.getString(ResourceTable.String_tw_pref_title_rate));
            checkupdate.setText(slice.getString(ResourceTable.String_tw_pref_title_check_update));
            versiontext.setText(slice.getString(ResourceTable.String_tw_app_version_text));
            settings.setText(slice.getString(ResourceTable.String_tw_drawer_item_settings));
        }
        String string = PreferenceUtil.getInstance().getString("titile1", "跟随系统");
        prefkeylanguage.setText(string);

        prefkeyabout.setClickedListener(this);
        prefkeylanguage.setClickedListener(this);
        back.setClickedListener(this);
        feedback.setClickedListener(this);
        report.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_pref_key_about:
                present(new AboutFraction(slice, new EditCallback() {
                    @Override
                    public void preview(int type, String s) {
                    }
                }), new Intent());
                break;
            case ResourceTable.Id_pref_key_language:
                languageShow(slice);
                break;
            case ResourceTable.Id_ig_ic_back:
                onBackPressed();
                break;
            case ResourceTable.Id_feedback:
                JudgeBean judgeBean = new JudgeBean();
                judgeBean.setType(INT_2);
                present(new WbeFraction(judgeBean), new Intent());
                break;
            case ResourceTable.Id_report:
                JudgeBean judgeBean1 = new JudgeBean();
                judgeBean1.setType(INT_5);
                present(new WbeFraction(judgeBean1), new Intent());
                break;
            case ResourceTable.Id_dl_auto:
                true1.setVisibility(Component.VISIBLE);
                setCacheStatic(0);
                if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
                    PreferenceUtil.getInstance().putString("titile1", "跟随系统");
                } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
                    PreferenceUtil.getInstance().putString("titile1", "Auto");
                }
                PreferenceUtil.getInstance().putInt("int", 0);
                onBackPressed();
                languageDialog.destroy();
                break;
            case ResourceTable.Id_dl_english:
                true2.setVisibility(Component.VISIBLE);
                PreferenceUtil.getInstance().putString("titile1", "English");
                PreferenceUtil.getInstance().putInt("int", 1);
                setCacheStatic(1);
                onBackPressed();
                languageDialog.destroy();
                break;
            case ResourceTable.Id_dl_chinese_s:
                true3.setVisibility(Component.VISIBLE);
                PreferenceUtil.getInstance().putString("titile1", "简体中文");
                PreferenceUtil.getInstance().putInt("int", INT_2);
                setCacheStatic(INT_2);
                onBackPressed();
                languageDialog.destroy();
                break;
            case ResourceTable.Id_dl_chinese_t:
                true4.setVisibility(Component.VISIBLE);
                PreferenceUtil.getInstance().putString("titile1", "繁體中文");
                PreferenceUtil.getInstance().putInt("int", INT_3);
                onBackPressed();
                languageDialog.destroy();
                break;
            default:
                break;
        }
    }

    /**
     * static赋值
     *
     * @param anInt static int type
     */
    public static void setCacheStatic(final int anInt) {
        StringCache.getInstance().setLanguageType(anInt);
    }

    /**
     * 显示方法
     *
     * @param context
     */
    public void languageShow(AbilitySlice context) {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(context).parse(ResourceTable.Layout_dialog_language_layout, null, false);

        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        customToastLayout.setLayoutConfig(layoutConfig);

        languageDialog = new CommonDialog(context);
        languageDialog.setContentCustomComponent(customToastLayout);
        languageDialog.setAutoClosable(true);
        languageDialog.setSize(ScreenUtils.getDisplayWidth(context) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        languageDialog.setAlignment(LayoutAlignment.CENTER);
        languageDialog.show();
        initView(customToastLayout);

        Text mContent = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_content);
        mContent.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                languageDialog.destroy();
            }
        });

        cutLanguage(titile1, ResourceTable.String_pref_array_item_auto,
                ResourceTable.String_en_pref_array_item_auto,
                ResourceTable.String_tw_pref_array_item_auto);
        cutLanguage(titile2, ResourceTable.String_pref_array_item_english,
                ResourceTable.String_en_pref_array_item_english,
                ResourceTable.String_tw_pref_array_item_english);
        cutLanguage(titile3, ResourceTable.String_pref_array_item_chinese_s,
                ResourceTable.String_en_pref_array_item_chinese_s,
                ResourceTable.String_tw_pref_array_item_chinese_s);
        cutLanguage(titile4, ResourceTable.String_pref_array_item_chinese_t,
                ResourceTable.String_en_pref_array_item_chinese_t,
                ResourceTable.String_tw_pref_array_item_chinese_t);
    }

    /**
     * 初始化控件
     *
     * @param customToastLayout
     */
    public void initView(ComponentContainer customToastLayout) {
        auto = (DependentLayout) customToastLayout.findComponentById(ResourceTable.Id_dl_auto);
        english = (DependentLayout) customToastLayout.findComponentById(ResourceTable.Id_dl_english);
        chineses = (DependentLayout) customToastLayout.findComponentById(ResourceTable.Id_dl_chinese_s);
        chineset = (DependentLayout) customToastLayout.findComponentById(ResourceTable.Id_dl_chinese_t);
        true1 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_true1);
        true2 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_true2);
        true3 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_true3);
        true4 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_true4);
        false1 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_false1);
        false2 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_false2);
        false3 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_false3);
        false4 = (Image) customToastLayout.findComponentById(ResourceTable.Id_ig_false4);
        titile1 = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_title1);
        titile2 = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_title2);
        titile3 = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_title3);
        titile4 = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_title4);
        true1.setClickedListener(this);
        true2.setClickedListener(this);
        true3.setClickedListener(this);
        true4.setClickedListener(this);
        false1.setClickedListener(this);
        false2.setClickedListener(this);
        false3.setClickedListener(this);
        false4.setClickedListener(this);
        auto.setClickedListener(this);
        english.setClickedListener(this);
        chineses.setClickedListener(this);
        chineset.setClickedListener(this);
    }

    private void cutLanguage(Text text, int chResource, int enResource, int twResource) {
        setCacheStatic(PreferenceUtil.getInstance().getInt("int", 0));
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            text.setText(slice.getString(chResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            text.setText(slice.getString(enResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.Chinesefont) {
            text.setText(slice.getString(twResource));
        }
    }
}
