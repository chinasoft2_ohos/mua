package io.github.zeleven.mua.adapter;

import io.github.zeleven.mua.ResourceTable;
import io.github.zeleven.mua.bean.FileBean;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

/**
 * ListContainerAdapter
 *
 * @since 2021-05-08
 */
public class ListContainerAdapter extends BaseItemProvider {
    private static final int INT_1000 = 1000;
    private static final int INT_60 = 60;
    private static final int INT_24 = 24;
    private List<FileBean> list;
    private Context context;

    /**
     * ListContainerAdapter
     *
     * @param list
     * @param context
     */
    public ListContainerAdapter(List<FileBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt;
        BaseViewHolder viewHolder;
        if (component == null) {
            cpt = LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_list_item_layout, null, false);
            viewHolder = new BaseViewHolder(cpt);
            cpt.setTag(viewHolder);
        } else {
            cpt = component;
            viewHolder = (BaseViewHolder) cpt.getTag();
        }
        viewHolder.title.setText(list.get(i).getTitle());
        viewHolder.content.setText(list.get(i).getContent().replaceAll("\n", ""));
        viewHolder.time.setText(list.get(i).getTime());
        int days = (int) ((list.get(i).getTime()) / (INT_1000 * INT_60 * INT_60 * INT_24));
        int hours = (int) ((list.get(i).getTime()) / (INT_1000 * INT_60 * INT_60));
        int minutes = (int) ((list.get(i).getTime()) / (INT_1000 * INT_60));

        if (Math.abs(days) > 0) {
            viewHolder.time.setText(Math.abs(days) + "天前");
        } else if (Math.abs(hours) > 0) {
            viewHolder.time.setText(Math.abs(hours) + "小时前");
        } else {
            viewHolder.time.setText(Math.abs(minutes) + "分钟前");
        }
        return cpt;
    }

    /**
     * BaseViewHolder
     *
     * @since 2021-05-15
     */
    private static class BaseViewHolder {
        Text title;
        Text content;
        Text time;

        /**
         * BaseViewHolder
         *
         * @param component
         */
        public BaseViewHolder(Component component) {
            title = (Text) component.findComponentById(ResourceTable.Id_item_title);
            content = (Text) component.findComponentById(ResourceTable.Id_item_content);
            time = (Text) component.findComponentById(ResourceTable.Id_item_time);
        }
    }
}
