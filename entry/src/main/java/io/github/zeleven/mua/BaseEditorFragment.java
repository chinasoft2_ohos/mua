package io.github.zeleven.mua;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * BaseEditorFragment
 *
 * @since 2021-05-11
 */
public abstract class BaseEditorFragment extends Fraction {
    protected boolean isSaved = false;
    protected boolean isFromFile = false;
    protected String fileName;
    protected String filePath;
    protected String fileContent;
    protected Ability context;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component view = scatter.parse(getLayoutId(), container, false);
        context = (Ability) getContext();
        initView(view);
        return view;
    }

    /**
     * initView
     *
     * @param view
     */
    public void initView(Component view) {
        if (!isFromFile) {
            isSaved = false;
            fileName = null;
            filePath = null;
            fileContent = null;
        }
    }

    /**
     * getLayoutId
     *
     * @return int
     */
    public abstract int getLayoutId();
}
