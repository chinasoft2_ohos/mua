package io.github.zeleven.mua.fraction;

import io.github.zeleven.mua.BaseEditorFragment;
import io.github.zeleven.mua.EditorAction;
import io.github.zeleven.mua.ResourceTable;
import io.github.zeleven.mua.bean.EditCallback;
import io.github.zeleven.mua.bean.FileBean;
import io.github.zeleven.mua.bean.StringCache;
import io.github.zeleven.mua.slice.EditAbilitySlice;
import io.github.zeleven.mua.utl.PreferenceUtil;
import io.github.zeleven.mua.utl.ReadFileUntils;
import io.github.zeleven.mua.utl.SaveDrawingTask;
import io.github.zeleven.mua.utl.ScreenUtils;
import io.github.zeleven.mua.utl.ToastUtil;
import io.github.zeleven.mua.view.BaseTextField;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Environment;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * EditFragment
 *
 * @since 2021-05-11
 */
public class EditFragment extends BaseEditorFragment implements Component.ClickedListener {
    private static final int INT = 0x001234;
    private static HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, INT, "zf------");
    private static final int SIX = 6;
    private static final int SEVEN = 7;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int INT_99 = 99;
    private BaseTextField contentInput;
    private EditorAction editorAction;
    private Image headingBtn;
    private Image boldBtn;
    private Image italicBtn;
    private Image blockCodeBtn;
    private Image quoteBtn;
    private Image listNumberBtn;
    private Image listBulletBtn;
    private Image linkBtn;
    private Image imageBtn;
    private Image back;
    private Image more;
    private Image preview;
    private Image undo;
    private Image redo;
    private Text rename;
    private Text delete;
    private Text save;
    private Text clear;
    private Text sheet;
    private Text statistics;
    private PopupDialog menuDialog;
    private EditCallback callback;
    private AbilitySlice slice;
    private String fileName;
    private Component menuComponent;
    private boolean isSave = false;
    private String renameString = "";
    private Text textlenght1;

    /**
     * EditFragment
     *
     * @param slice
     * @param callback
     */
    public EditFragment(AbilitySlice slice, EditCallback callback) {
        this.slice = slice;
        this.callback = callback;
    }

    @Override
    public int getLayoutId() {
        return ResourceTable.Layout_fragment_edit;
    }

    /***
     * setContent
     *
     * @param content
     */
    public void setContent(String content) {
        contentInput.setText(content);
    }

    /**
     * setFileName
     *
     * @param isMsave
     */
    public void setFileName(boolean isMsave) {
        this.isSave = isMsave;
    }

    /**
     * setSave
     *
     * @param isMsave
     */
    public void setSave(boolean isMsave) {
        this.isSave = isMsave;
    }

    @Override
    public void initView(Component view) {
        super.initView(view);
        contentInput = (BaseTextField) view.findComponentById(ResourceTable.Id_content_input);
        headingBtn = (Image) view.findComponentById(ResourceTable.Id_heading);
        boldBtn = (Image) view.findComponentById(ResourceTable.Id_bold);
        italicBtn = (Image) view.findComponentById(ResourceTable.Id_italic);
        blockCodeBtn = (Image) view.findComponentById(ResourceTable.Id_code);
        quoteBtn = (Image) view.findComponentById(ResourceTable.Id_quote);
        listNumberBtn = (Image) view.findComponentById(ResourceTable.Id_list_number);
        listBulletBtn = (Image) view.findComponentById(ResourceTable.Id_list_bullet);
        linkBtn = (Image) view.findComponentById(ResourceTable.Id_link);
        imageBtn = (Image) view.findComponentById(ResourceTable.Id_image);
        back = (Image) view.findComponentById(ResourceTable.Id_ic_back_img);
        more = (Image) view.findComponentById(ResourceTable.Id_ic_more_img);
        preview = (Image) view.findComponentById(ResourceTable.Id_ic_preview_img);
        undo = (Image) view.findComponentById(ResourceTable.Id_ic_undo_img);
        redo = (Image) view.findComponentById(ResourceTable.Id_ic_redo_img);

        if (fileContent != null) {
            contentInput.setText(fileContent);
        }
        if (editorAction == null) {
            editorAction = new EditorAction();
        }
        editorAction = new EditorAction(slice, contentInput);
        contentInput.requestFocus();

        if (!StringCache.getInstance().getString().isEmpty() || !StringCache.getInstance().getString().equals("")) {
            contentInput.setText(StringCache.getInstance().getString());
        }
        setOnClickListener();
    }

    @Override
    public void onClick(Component v) {
        switch (v.getId()) {
            case ResourceTable.Id_heading:
                editorAction.heading();
                break;
            case ResourceTable.Id_bold:
                editorAction.bold();
                break;
            case ResourceTable.Id_italic:
                editorAction.italic();
                break;
            case ResourceTable.Id_code:
                editorAction.insertCode();
                break;
            case ResourceTable.Id_quote:
                editorAction.quote();
                break;
            case ResourceTable.Id_list_number:
                editorAction.orderedList();
                break;
            case ResourceTable.Id_list_bullet:
                editorAction.unorderedList();
                break;
            case ResourceTable.Id_link:
                editorAction.insertLink();
                break;
            case ResourceTable.Id_image:
                showImageDialog();
                break;
            case ResourceTable.Id_ic_preview_img:
                HiLog.info(hiLogLabel, "" + contentInput.getText());
                callback.preview(1, contentInput.getText());
                setCacheStatic(contentInput.getText());
                break;
            case ResourceTable.Id_ic_back_img:
                callback.preview(THREE, "");
                break;
            case ResourceTable.Id_ic_more_img:
                showMenuDialog();
                break;
            case ResourceTable.Id_menu_save:
                if (!isSave && fileName == null) {
                    menuDialog.remove();
                    showSaveDialog();
                } else if (isSave) {
                    saveContent();
                    menuDialog.remove();
                    clearFocus(menuComponent);
                } else {
                    menuDialog.remove();
                    clearFocus(menuComponent);
                }
                break;
            case ResourceTable.Id_menu_rename:
                if (isSave) {
                    menuDialog.remove();
                    showRenameDialog();
                }
                break;
            case ResourceTable.Id_menu_delete:
                if (isSave) {
                    menuDialog.remove();
                    showDeleteDialog();
                }
                break;
            case ResourceTable.Id_menu_clear:
                menuDialog.remove();
                if (contentInput.getText().length() == 0) {
                    ToastUtil.toast(slice, "内容为空");
                } else {
                    showClearDialog();
                }
                break;
            case ResourceTable.Id_menu_sheet:
                menuDialog.remove();
                callback.preview(FOUR, "");
                break;
            case ResourceTable.Id_menu_statistics:
                showStatisticsDialog();
                menuDialog.remove();
                break;
            case ResourceTable.Id_ic_redo_img:
                editorAction.redo();
                break;
            case ResourceTable.Id_ic_undo_img:
                editorAction.undo();
                break;
            case ResourceTable.Id_content_input:
                break;
            default:
                break;
        }
    }

    /**
     * static赋值
     *
     * @param strText static type
     */
    public static void setCacheStatic(final String strText) {
        StringCache.getInstance().setString(strText);
    }

    private void saveContent() {
        SaveDrawingTask.setSavedImageName(StringCache.getInstance().getFileName().replaceAll(".md", ""));
        SaveDrawingTask saveDrawingTask = new SaveDrawingTask((EditAbilitySlice) slice);
        saveDrawingTask.setValue(contentInput.getText().replaceAll("\n", "___"));
        new Thread(saveDrawingTask).start();
    }

    private void showMenuDialog() {
        menuComponent = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_menu_layout, null, false);
        save = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_save);
        rename = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_rename);
        delete = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_delete);
        clear = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_clear);
        sheet = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_sheet);
        statistics = (Text) menuComponent.findComponentById(ResourceTable.Id_menu_statistics);
        menuDialog = new PopupDialog(slice, null);
        menuDialog.setCustomComponent(menuComponent);
        menuDialog.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.TOP);
        menuDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        menuDialog.setAutoClosable(true);
        menuDialog.show();
        save.setClickedListener(this);
        clear.setClickedListener(this);
        rename.setClickedListener(this);
        delete.setClickedListener(this);
        sheet.setClickedListener(this);
        statistics.setClickedListener(this);
        cutLanguage(save, ResourceTable.String_save, ResourceTable.String_en_save, ResourceTable.String_tw_save);
        cutLanguage(rename, ResourceTable.String_rename, ResourceTable.String_en_rename, ResourceTable.String_tw_rename);
        cutLanguage(delete, ResourceTable.String_delete, ResourceTable.String_en_delete, ResourceTable.String_tw_delete);
        cutLanguage(clear, ResourceTable.String_clear, ResourceTable.String_en_clear, ResourceTable.String_tw_clear);
        cutLanguage(sheet, ResourceTable.String_sheet, ResourceTable.String_en_sheet, ResourceTable.String_tw_sheet);
        cutLanguage(statistics, ResourceTable.String_statistics, ResourceTable.String_en_statistics, ResourceTable.String_tw_statistics);
        if (isSave) {
            rename.setTextColor(Color.BLACK);
            delete.setTextColor(Color.BLACK);
        }
    }

    private void showDeleteDialog() {
        File file = new File(slice.getApplicationContext().getExternalFilesDir(
                Environment.DIRECTORY_DOCUMENTS), StringCache.getInstance().getFileName() + ".md");
        boolean isExists = file.exists();
        if (isExists) {
            boolean isDelete = file.delete();
            if (isDelete) {
                new ToastDialog(slice).setContentText("删除成功").show();
            }
        }
    }

    private void showRenameDialog() {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(slice).parse(ResourceTable.Layout_save_layout, null, false);
        Text title = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_title);
        TextField textField = (TextField) customToastLayout.findComponentById(ResourceTable.Id_save_fileName);
        cutLanguage(title, ResourceTable.String_dialog_title_save_file,
                ResourceTable.String_en_dialog_title_save_file,
                ResourceTable.String_tw_dialog_title_save_file);
        cutLanguage(textField, ResourceTable.String_dialog_et_hint_file_name,
                ResourceTable.String_en_dialog_et_hint_file_name,
                ResourceTable.String_tw_dialog_et_hint_file_name);
        CommonDialog toastDialog = new CommonDialog(slice);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(slice) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();

        if (!renameString.isEmpty() || !renameString.equals("")) {
            textField.setText(renameString);
        }
        Text right = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_right);
        cutLanguage(right, ResourceTable.String_right, ResourceTable.String_en_right, ResourceTable.String_tw_right);
        right.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.remove();
                renameString = textField.getText();
                List<FileBean> list = new ArrayList<>();
                ReadFileUntils.readFile(list, slice);
                String editString = textField.getText() + ".md";
                for (FileBean fileBean : list) {
                    if (editString.equals(fileBean.getTitle())) {
                        new ToastDialog(slice).setContentText("文件名已存在,请重新输入").show();
                        HiLog.info(hiLogLabel, "文件名称" + fileBean.getContent());
                        return;
                    }
                }
                renameFile(editString);
            }
        });
        Text cancel = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_cancel);
        cutLanguage(cancel, ResourceTable.String_cancel, ResourceTable.String_en_cancel, ResourceTable.String_tw_cancel);
        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.remove();
            }
        });
    }

    private void renameFile(String newName) {
        File oldFile = new File(slice.getApplicationContext().getExternalFilesDir(
                Environment.DIRECTORY_DOCUMENTS), StringCache.getInstance().getFileName() + ".md");
        boolean isExists = oldFile.exists();
        if (isExists) {
            File newFile = new File(slice.getApplicationContext().getExternalFilesDir(
                    Environment.DIRECTORY_DOCUMENTS), newName);
            boolean isRename = oldFile.renameTo(newFile);
        }
    }

    private void showClearDialog() {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(slice).parse(ResourceTable.Layout_clear_layout, null, false);
        CommonDialog toastDialog = new CommonDialog(slice);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(slice) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
        Text right = (Text) customToastLayout.findComponentById(ResourceTable.Id_clear_right);
        Text title = (Text) customToastLayout.findComponentById(ResourceTable.Id_clearall);
        cutLanguage(right, ResourceTable.String_right, ResourceTable.String_en_right, ResourceTable.String_tw_right);
        cutLanguage(title, ResourceTable.String_dialog_message_clear_all,
                ResourceTable.String_en_dialog_message_clear_all,
                ResourceTable.String_tw_dialog_message_clear_all);
        right.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                contentInput.setText("");
                toastDialog.remove();
            }
        });
        Text cancel = (Text) customToastLayout.findComponentById(ResourceTable.Id_clear_cancel);
        cutLanguage(cancel, ResourceTable.String_cancel, ResourceTable.String_en_cancel, ResourceTable.String_tw_cancel);
        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.remove();
            }
        });
    }

    private void showSaveDialog() {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(slice).parse(ResourceTable.Layout_save_layout, null, false);
        CommonDialog toastDialog = new CommonDialog(slice);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(slice) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
        TextField textField = (TextField) customToastLayout.findComponentById(ResourceTable.Id_save_fileName);
        Text right = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_right);
        Text title = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_title);
        cutLanguage(right, ResourceTable.String_right, ResourceTable.String_en_right, ResourceTable.String_tw_right);
        cutLanguage(textField, ResourceTable.String_dialog_et_hint_file_name,
                ResourceTable.String_en_dialog_et_hint_file_name,
                ResourceTable.String_tw_dialog_et_hint_file_name);
        cutLanguage(title, ResourceTable.String_dialog_title_save_file,
                ResourceTable.String_en_dialog_title_save_file,
                ResourceTable.String_tw_dialog_title_save_file);

        right.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                HiLog.info(hiLogLabel, contentInput.getText());
                StringCache.getInstance().setFileName(textField.getText());
                if (textField.getText().isEmpty() || textField.getText().equals("")) {
                    ToastUtil.toast(slice, "文件名不能为空");
                    toastDialog.remove();
                } else {
                    SaveDrawingTask.setSavedImageName(textField.getText());
                    saveFile(textField);
                    toastDialog.remove();
                }
            }
        });
        Text cancel = (Text) customToastLayout.findComponentById(ResourceTable.Id_save_cancel);
        cutLanguage(cancel, ResourceTable.String_cancel, ResourceTable.String_en_cancel, ResourceTable.String_tw_cancel);
        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.remove();
            }
        });
    }

    private void saveFile(TextField textField) {
        List<FileBean> list = new ArrayList<>();
        ReadFileUntils.readFile(list, slice);
        for (FileBean fileBean : list) {
            if (fileBean.getTitle().equals(StringCache.getInstance().getFileName() + ".md")) {
                new ToastDialog(slice).setContentText("文件名已存在,请重新输入").show();
                return;
            }
        }
        isSave = true;
        renameString = textField.getText();
        rename.setTextColor(Color.BLACK);
        delete.setTextColor(Color.BLACK);
        ToastUtil.toast(slice, "已保存");
        SaveDrawingTask saveDrawingTask = new SaveDrawingTask((EditAbilitySlice) slice);
        saveDrawingTask.setValue(contentInput.getText().replaceAll("\n", "___"));
        new Thread(saveDrawingTask).start();
    }

    private void showStatisticsDialog() {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(slice).parse(ResourceTable.Layout_statistics_layout, null, false);
        textlenght1 = (Text) customToastLayout.findComponentById(ResourceTable.Id_statistics_string_num1);
        HiLog.info(hiLogLabel, "--------=" + contentInput.getText().length());
        int length = contentInput.getText().length();
        textlenght1.setText(length + "");
        Text title = (Text) customToastLayout.findComponentById(ResourceTable.Id_statistics_title);
        Text sum = (Text) customToastLayout.findComponentById(ResourceTable.Id_statistics_sum_strings);
        Text textlenght = (Text) customToastLayout.findComponentById(ResourceTable.Id_statistics_string_num);
        cutLanguage(textlenght, ResourceTable.String_only_strings, ResourceTable.String_en_only_strings, ResourceTable.String_tw_only_strings);
        cutLanguage(title, ResourceTable.String_statistics, ResourceTable.String_en_statistics, ResourceTable.String_tw_statistics);
        cutLanguage(sum, ResourceTable.String_all_strings, ResourceTable.String_en_all_strings, ResourceTable.String_tw_all_strings);
        CommonDialog toastDialog = new CommonDialog(slice);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(slice) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
        Text right = (Text) customToastLayout.findComponentById(ResourceTable.Id_statistics_right);
        cutLanguage(right, ResourceTable.String_right, ResourceTable.String_en_right, ResourceTable.String_tw_right);
        right.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.hide();
                toastDialog.remove();
            }
        });
    }

    private void showImageDialog() {
        ComponentContainer customToastLayout = (ComponentContainer) LayoutScatter
                .getInstance(slice).parse(ResourceTable.Layout_dialog_insert_link, null, false);
        final Text title = (Text) customToastLayout.findComponentById(ResourceTable.Id_insert_link);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        customToastLayout.setLayoutConfig(layoutConfig);

        final BaseTextField imgurl = (BaseTextField) customToastLayout.findComponentById(ResourceTable.Id_link_content);
        final BaseTextField imgDisplay = (BaseTextField) customToastLayout.
                findComponentById(ResourceTable.Id_link_display_text);
        CommonDialog toastDialog = new CommonDialog(slice);
        toastDialog.setContentCustomComponent(customToastLayout);
        toastDialog.setAutoClosable(true);
        toastDialog.setSize(ScreenUtils.getDisplayWidth(slice) / SEVEN * SIX,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.show();
        final Text cancel = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_cancel);
        cutLanguage(title, ResourceTable.String_dialog_title_insert_link,
                ResourceTable.String_en_dialog_title_insert_link,
                ResourceTable.String_tw_dialog_title_insert_link);
        cutLanguage(imgurl, ResourceTable.String_dialog_et_hint_link_content,
                ResourceTable.String_en_dialog_et_hint_link_content,
                ResourceTable.String_tw_dialog_et_hint_link_content);
        cutLanguage(imgDisplay, ResourceTable.String_dialog_show_text, ResourceTable.String_en_dialog_show_text, ResourceTable.String_tw_dialog_et_hint_link_display_text);
        cutLanguage(cancel, ResourceTable.String_cancel, ResourceTable.String_en_cancel, ResourceTable.String_tw_cancel);
        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toastDialog.remove();
            }
        });
        Text insertText = (Text) customToastLayout.findComponentById(ResourceTable.Id_tv_insert);
        cutLanguage(insertText, ResourceTable.String_insert, ResourceTable.String_en_insert, ResourceTable.String_tw_dialog_btn_insert);
        insertText.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                editorAction.insertImage(imgDisplay.getText(), imgurl.getText());
                toastDialog.remove();
                contentInput.setStartPosition(contentInput.getText().length());
                contentInput.setEndPosition(contentInput.getText().length());
            }
        });
    }

    /**
     * setOnClickListener
     */
    private void setOnClickListener() {
        headingBtn.setClickedListener(this);
        boldBtn.setClickedListener(this);
        italicBtn.setClickedListener(this);
        blockCodeBtn.setClickedListener(this);
        quoteBtn.setClickedListener(this);
        listNumberBtn.setClickedListener(this);
        listBulletBtn.setClickedListener(this);
        linkBtn.setClickedListener(this);
        imageBtn.setClickedListener(this);
        preview.setClickedListener(this);
        back.setClickedListener(this);
        more.setClickedListener(this);
        redo.setClickedListener(this);
        undo.setClickedListener(this);
        contentInput.setClickedListener(this);
    }

    /**
     * clearFocus
     *
     * @param component
     */
    private void clearFocus(Component component) {
        Component focusedComponent = findFocus(component, Component.FOCUS_NEXT);
        if (focusedComponent != null) {
            focusedComponent.clearFocus();
            return;
        }

        focusedComponent = findFocus(component, Component.FOCUS_PREVIOUS);
        if (focusedComponent != null) {
            focusedComponent.clearFocus();
        }
    }

    /**
     * findFocus
     *
     * @param component
     * @param direction
     * @return Component
     */
    private Component findFocus(Component component, int direction) {
        if (component.hasFocus()) {
            return component;
        }
        Component focusableComponent = component;
        int i = INT_99;
        while (i-- > 0) {
            focusableComponent = focusableComponent.findNextFocusableComponent(direction);
            if (focusableComponent != null) {
                if (focusableComponent.hasFocus()) {
                    return focusableComponent;
                }
            } else {
                break;
            }
        }
        return null;
    }

    private void cutLanguage(Text text, int chResource, int enResource, int twResource) {
        int anInt = PreferenceUtil.getInstance().getInt("int", 0);
        StringCache.getInstance().setLanguageType(anInt);
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            text.setText(slice.getString(chResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            text.setText(slice.getString(enResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.Chinesefont) {
            text.setText(slice.getString(twResource));
        }
    }

    private void cutLanguage(TextField text, int chResource, int enResource, int twResource) {
        int anInt = PreferenceUtil.getInstance().getInt("int", 0);
        StringCache.getInstance().setLanguageType(anInt);
        if (StringCache.getInstance().getLanguageType() == StringCache.Default || StringCache.getInstance().getLanguageType() == StringCache.Chinese) {
            text.setHint(slice.getString(chResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.English) {
            text.setHint(slice.getString(enResource));
        } else if (StringCache.getInstance().getLanguageType() == StringCache.Chinesefont) {
            text.setHint(slice.getString(twResource));
        }
    }
}
