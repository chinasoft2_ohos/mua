package io.github.zeleven.mua.fraction;

import io.github.zeleven.mua.ResourceTable;
import io.github.zeleven.mua.bean.EditCallback;
import io.github.zeleven.mua.bean.JudgeBean;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.webengine.WebView;

/**
 * WbeFraction
 *
 * @since 2021-05-12
 */
public class WbeFraction extends AbilitySlice {
    private static final int INT_2 = 2;
    private static final int INT_3 = 3;
    private static final int INT_4 = 4;
    private static final int INT_5 = 5;
    private WebView web;
    private JudgeBean mJudgeBean;

    /**
     * WbeFraction
     *
     * @param mjudgeBean
     */
    public WbeFraction(JudgeBean mjudgeBean) {
        this.mJudgeBean = mjudgeBean;
    }

    /**
     * PreviewFragment
     *
     * @param slice
     * @param callback
     */
    public WbeFraction(AbilitySlice slice, EditCallback callback) {
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_fragment_web);
        web = (WebView) findComponentById(ResourceTable.Id_web);
        web.getWebConfig().setJavaScriptPermit(true);
        int type = mJudgeBean.getType();
        if (type == 1) {
            web.load("https://github.com/zeleven/mua");
        } else if (type == INT_2) {
            web.load("https://www.baidu.com/");
        } else if (type == INT_3) {
            web.load("https://github.com/zeleven");
        } else if (type == INT_4) {
            web.load("https://www.zhihu.com/people/zheng-zheng-shu-36");
        } else if (type == INT_5) {
            web.load("https://github.com/zeleven/mua/issues");
        }
    }
}
